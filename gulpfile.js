/////////////////////////////////
//	$REQUIRE
var gulp         = require('gulp');
var browserSync  = require('browser-sync').create();
var plumber      = require('gulp-plumber');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

///////////////	$TASKS ///////////////

// SCSSS --> CSS
gulp.task('sass', function(){
	return gulp.src('app/css/app.scss')
	.pipe(plumber())
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(gulp.dest('app/css/'))
	.pipe(browserSync.stream());
});

//	WATCH --> HTML-SCSS-JS
gulp.task('default', ['sass'], function(){
	browserSync.init({
		notify: false,
    server: {
			baseDir: '/var/www/html/lab/responsive-menu/app/'
		}
  });
	gulp.watch('app/css/**/*.scss', ['sass']).on('change', function(event){
  	// affiche quel fichier SCSS est mofifié
  	console.log('le fichier ' + event.path + ' a été modifié')
  });
	gulp.watch('app/js/**/*.js').on('change', browserSync.reload).on('change', function(event){
  	// affiche quel fichier SCSS est mofifié
  	console.log('le fichier ' + event.path + ' a été modifié')
  });
  gulp.watch('app/*.html').on('change', browserSync.reload).on('change', function(event){
  	// affiche quel fichier HTML est mofifié
  	console.log('le fichier ' + event.path + ' a été modifié')
  });
});
